package main

type Account struct {
	Country string							`json:"country"`
	BaseCurrency string						`json:"base_currency"`
	BankId string							`json:"bank_id"`
	BankIdCode string						`json:"bank_id_code"`
	AccountNumber string					`json:"account_number"`
	Bic string								`json:"bic"`
	Iban string								`json:"iban"`
	CustomerId string						`json:"customer_id"`
	Name []string							`json:"name"`
	AlternativeNames [3]string				`json:"alternative_names"`
	AccountClassification string			`json:"account_classification"`
	JointAccount bool						`json:"joint_account"`
	AccountMatchingOptOut bool				`json:"account_matching_opt_out"`
	SecondaryIdentification string			`json:"secondary_identification"`
	Switched bool							`json:"switched"`
	Status string							`json:"status"`
	Title string 							`json:"title"`	//deprecated
	FirstName string 						`json:"first_name"`  //deprecated
	BankAccountName string 					`json:"bank_account_name"`	//deprecateed
	AlternativeBankAccountNames [3]string 	`json:"alternative_bank_account_names"`//deprecated
}
