package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/net/context"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	ProdURL = "https://api.company.com"
	TestURL = "http://localhost:8080"
)

type Client struct {
	BaseURL    string
	APIVersion string
	token     string
	HTTPClient *http.Client
}

type APIEnvironment string

const (
	Production APIEnvironment = "https://api3.company.tech"
	Sandbox APIEnvironment = "http://localhost:8080"
)

type ListAccountOptions struct {
	Limit int
	Page  int
}

func NewClient(environment APIEnvironment, token string) *Client {
	return &Client{
		BaseURL: string(environment),
		token:  token,
		APIVersion: "1",
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

func (c* Client) GetAPIVersion() (string) {
	return c.APIVersion
}

func (c* Client) SetAPIVersion(num string) {
	c.APIVersion = num
}

func (c* Client) CreateAccount(ctx context.Context, account *Account) (*AccountCreateResponse, error) {
	create_account_url := "/v1/organisation/accounts"
	req, err := http.NewRequest("POST", c.BaseURL + create_account_url, nil)
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)
	createResponse := AccountCreateResponse{}
	var body io.ReadCloser

	body, err = c.sendRequest(req, account)

	if err != nil {
		return nil, err
	}
	defer body.Close()

	if err = json.NewDecoder(body).Decode(&createResponse); err != nil {
		return nil, err
	}

	return &createResponse, nil

	return nil, nil
}

func (c* Client) FetchAccount(ctx context.Context, account *Account) (*AccountFetchResponse, error) {
	return nil, nil
}

func (c *Client) sendRequest(req *http.Request, body interface{}) (io.ReadCloser, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", c.token))

	var err error
	var bufBytes []byte

	if body != nil {
		bufBytes, err = json.Marshal(body)
		if err != nil {
			return nil, err
		}

		var body io.Reader
		body = bytes.NewReader(bufBytes)

		rc, ok := body.(io.ReadCloser)
		if !ok && body != nil {
			rc = ioutil.NopCloser(body)
		}
		req.Body = rc
	}
	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		return nil, fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	return res.Body, nil
}

func (c* Client) ListAccounts(ctx context.Context, options *ListAccountOptions) (*AccountListResponse, error) {
	var err error
	limit := 100
	page := 0
	if options != nil {
		limit = options.Limit
		page = options.Page
	}

	list_accounts_url := "/v%s/organisation/accounts?page[number]=%d&page[size]=%d"
	list_accounts_url = fmt.Sprintf(list_accounts_url, c.APIVersion, page, limit)

	req, err := http.NewRequest("GET", c.BaseURL + list_accounts_url, nil)
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)
	listResponse := AccountListResponse{}
	var body io.ReadCloser

	body, err = c.sendRequest(req, nil)

	if err != nil {
		return nil, err
	}
	defer body.Close()

	if err = json.NewDecoder(body).Decode(&listResponse); err != nil {
		return nil, err
	}

	return &listResponse, nil
}

func (c* Client) DeleteAccount(ctx context.Context) (error) {
	return nil
}