package main

import (
	"fmt"
	"golang.org/x/net/context"
	"os"
)

func main() {
	fmt.Println("Hello")
	token := "WktJQUpDRjVVSENaQkNLTENXNVE6ZGEyZjNmOTdhNTAxNGVjZWI1NzQ1MzZlOTc2NTJjNTY="
	c := NewClient(TestURL, token)
	ctx := context.Background()
	res, err := c.ListAccounts(ctx, nil)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	fmt.Println(res)


	acc :=  Account{
		Country: "GB",
		BaseCurrency: "GBP",
		BankId: "400300",
		BankIdCode: "GBDSC",
		Bic: "NWBKGB22",
	}
	response, err := c.CreateAccount(ctx, &acc)
	if err != nil {
		fmt.Println("ERROR" , err)
		os.Exit(-1)
	}
	fmt.Println(response)

}
