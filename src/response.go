package main

import "time"

type ResourceDefaultAttributes struct {
	Id string `json:"id"`
	OrganisationId string `json:"organisation_id"`
	Type string	`json:"type"`
	Version int	`json:"version"`
	CreatedOn time.Time `json:"created_on"`
	ModifiedOn time.Time `json:"modified_on"`
}

type AccountResource struct {
	ResourceDefaultAttributes
	Attributes Account `json:"attributes"`
	Relationships interface{} `json:"relationships"`
}

type errorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type LinksSection struct {
	Self string `json:"self"`
	First string `json:"first"`
	Last string `json:"last"`
	Next string `json:"next"`
	Prev string `json:"prev"`
}

type AccountListResponse struct {
	Data []AccountResource `json:"data"`
	Links LinksSection `json:"links"`
}

type AccountFetchResponse struct {
	Data AccountResource `json:"data"`
	Links LinksSection `json:"links"`
}

type AccountCreateResponse struct {
	Data AccountResource `json:"data"`
	Links LinksSection `json:"links"`
}


